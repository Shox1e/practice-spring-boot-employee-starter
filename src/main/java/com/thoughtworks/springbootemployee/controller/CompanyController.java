package com.thoughtworks.springbootemployee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private List<Company> companyList= new ArrayList<>();
    @Resource
    private EmployeeResitory employeeResitory;

    @GetMapping
    public List<Company> getCompanyList() {
        return companyList;
    }

    @GetMapping("{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyList.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    @PutMapping("employees/{id}")
    public Employee putEmployee(@PathVariable Long id){
        Employee employee = employeeResitory.getEmployeeById(id);
        employee.setCompanyId(1L);
        return employee;
    }

    @GetMapping("{id}/employees")
    public List<Employee> obtainEmployeeList(@PathVariable Long id){
        return employeeResitory.getEmployees().stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }

    @GetMapping(params = {"page","size"})
    public List<Company> querryEmployeeList(Integer page,Integer size){
        return companyList.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }

    @PostMapping
    public Company addCompany(@RequestBody Company company){
        companyList.add(company);
        return company;
    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id){
        Company company = companyList.stream().filter(needUpdateCompany -> needUpdateCompany.getId().equals(id)).findFirst().orElse(null);
        if (!Objects.isNull(company)){
            company.setName("new Name");
        }
        return company;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        employeeResitory.getEmployees().forEach(employee -> {
            if (employee.getCompanyId().equals(id)){
                employee.setCompanyId(null);
            }
        });
        this.companyList=companyList.stream().filter(company -> !Objects.equals(company.getId(), id)).collect(Collectors.toList());
    }
}
