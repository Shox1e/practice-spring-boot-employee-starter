package com.thoughtworks.springbootemployee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    private List<Employee> employeelist = new ArrayList<>();
    @Resource
    private EmployeeResitory employeeResitory;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        employee.setId(generateId());
        this.employeeResitory.getEmployees().add(employee);
        return employee;
    }

    @GetMapping("/get")
    public List<Employee> getEmployeeList() {
        return this.employeeResitory.getEmployees();
    }

    @GetMapping("/getById/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeResitory.getEmployees().stream().filter(employee -> Objects.equals(employee.getId(), id)).findFirst().orElse(null);
    }

    @GetMapping("/getByGender")
    public Employee getEmployeeById(@RequestParam String gender) {
        return employeeResitory.getEmployees().stream().filter(employee -> Objects.equals(employee.getGender(), gender)).findFirst().orElse(null);
    }

    @PutMapping("/updateEmployee/{id}")
    public Employee updateEmployee(@PathVariable Long id) {
        Employee needUpdateEmployee = employeeResitory.getEmployees().stream().filter(employee -> Objects.equals(employee.getId(), id)).findFirst().orElse(null);
        needUpdateEmployee.setSalary(10);
        needUpdateEmployee.setAge(1);
        return needUpdateEmployee;
    }

    @DeleteMapping("/deleteEmployee/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        this.employeeResitory.setEmployees(employeeResitory.getEmployees().stream().filter(employee -> !Objects.equals(employee.getId(), id)).collect(Collectors.toList()));
    }

    @GetMapping("/getEmployeeByPage")
    public List<Employee> getEmployeeByPage(@RequestParam Integer page,@RequestParam Integer size) {
        return employeeResitory.getEmployees().stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }
    public Long generateId(){
        return employeeResitory.getEmployees().stream().max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId()+1).orElse(1L);
    }
}
