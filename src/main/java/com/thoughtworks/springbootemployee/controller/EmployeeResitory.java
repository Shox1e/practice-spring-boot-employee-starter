package com.thoughtworks.springbootemployee.controller;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
@Repository
public class EmployeeResitory {
    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    private List<Employee> employees = new ArrayList<>();
    public Employee save(Employee employee){
        employee.setId(generateId());
        this.employees.add(employee);
        return employee;
    }

    public Long generateId(){
        return employees.stream().max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId()+1).orElse(1L);
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().orElse(null);
    }
}
